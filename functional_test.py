from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

class Story6FunctionalTest(unittest.TestCase):
	def setUp(self):
		# chrome_options = Options()
		# chrome_options.add_argument('--dns-prefetch-disable')
		# chrome_options.add_argument('no-sandbox')
		# chrome_options.add_argument('--headless')
		# chrome_options.add_argument('disable-gpu')
		# self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.browser = webdriver.Chrome('./chromedriver')
		# super(Story6FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		# super(Story6FunctionalTest, self).tearDown()

	def test_status_input(self):
		# get the website 
		# self.browser.get('http://ppw-c-azhardifaarnanda.herokuapp.com/story6/status')
		self.browser.get('http://localhost:8000/story6/status')
		# website backbone check
		self.assertIn('functional_test_story', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertTrue('Oh, hello Boy!' == header_text)
		# POST test
		input1 = 'loremipsum'
		myStatus = self.browser.find_element_by_id('id_Kabar')
		myStatus.send_keys(input1)
		myStatus.submit()
		# GET test
		post1 = self.browser.find_element_by_xpath("//p[@id='my_status']").text
		self.assertIn(input1, post1)

	def test_positioning(self):
		self.browser.get('http://localhost:8000/story6/status')
		form = self.browser.find_element_by_id('id_Kabar')
		self.assertEquals({'x':425,'y':128}, form.location)

	def test_positioning_2(self):
		self.browser.get('http://localhost:8000/story6/status')
		post_card = self.browser.find_element_by_class_name('card')
		self.assertEquals({'x':425,'y':384}, post_card.location)

	def test_css(self):
		self.browser.get('http://localhost:8000/story6/status')
		button = self.browser.find_element_by_name("post_button")
		self.assertEquals('btn', button.get_attribute('class'))

		
if __name__ == '__main__':#
	unittest.main(warnings='ignore')
		
