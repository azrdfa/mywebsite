from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Status
from .forms import Status_Message

def story6_status(request):
	status = Status.objects.all().values()
	form = Status_Message()
	if status.count() != 0:
		temp = []
		i = status.count() - 1
		while i >= 0:
			temp.append(status[i])
			i -= 1
			
		return render(request, "status.html", {'status' : temp, 'form' : form})	

	return render(request, "status.html", {'status' : status, 'form' : form})

def story6_status_add(request):
	if request.method == "POST":
		form = Status_Message(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/story6/status/')

def story6_status_del(request):
	Status.objects.all().delete()
	return redirect('/story6/status/')

def story8(request):
	return render(request, "coba.html")



	