from django.urls import path
from status import views

urlpatterns = [
    path('status/', views.story6_status, name="status"),
    path('status/add_activity', views.story6_status_add, name="status_add"),
    path('status/delete_activity', views.story6_status_del, name="status_del"),
    path('story8', views.story8, name="javascript")
] 
