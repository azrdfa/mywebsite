$(document).ready(function(){
	var $mybooks = $('#appended');
	$.ajax({
		type: 'GET',
		url: '/story9/json_data',
		success: function(data) {
			console.log('success', data);
			$.each(data.items, function(i, books){
				$mybooks.append('<tr>');
				$mybooks.append('<th scope="row">'+(i+1)+'</th>');
				$mybooks.append('<td><img src="'+books.volumeInfo.imageLinks.thumbnail+'"/></td>');
				$mybooks.append('<td>'+books.volumeInfo.title+'</td>');
				$mybooks.append('<td>'+books.volumeInfo.authors+'</td>');
				$mybooks.append('<td>'+books.volumeInfo.publisher+'</td>');
				$mybooks.append('<td><i class="fas fa-check-circle" style="font-size:48px;color:rgb(0, 0, 0)"></i></td>'); 
				$mybooks.append('</tr>');
			});
		},
		error: function() {
			alert('error loading orders');
		}
	});

});
$(document).ready(function(){
	var color = ["rgb(0, 0, 0)", "rgb(100, 132, 12)"]
	var count = 0
	$('body').on('click', 'i', function() {
		var $col1 = $(this).css('color')
		if ($col1 == color[0]) {
			$(this).css('color', color[1])
			count++
		}
		else {
			$(this).css('color', color[0])	
			count--
		}
		$('#clicks').text(count);
	});	
});