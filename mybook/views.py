from django.shortcuts import render, reverse
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from .models import Registration
from .forms import Subscribe_Form
import requests
import json
from django.core import serializers
from django.contrib.auth import authenticate, login

# Create your views here.
response = {}
def book_func(request):
	return render(request, 'mybooklist.html', response);

def json_data(request, book):
	URL = 'https://www.googleapis.com/books/v1/volumes?q={}'.format(book)
	REQ = requests.get(url = URL)
	JSON = REQ.json()
	return JsonResponse(JSON)

def subs_func(request):
	html = 'subscriber.html'
	return render(request, html)

def status_func(request):
	html = 'status.html'
	return render(request, html)

def submit_func(request):
	username_info = request.POST.get('username')
	email_info = request.POST.get('email')
	pass_info = request.POST.get('password')
	object_save = Registration(username=username_info, email=email_info, password=pass_info)
	object_save.save();
	return HttpResponse(request)


def email_check(request, email):
	mydata = {'status': False} 
	mydata['status'] = Registration.objects.filter(email__iexact=email).exists()

	if mydata['status']:
		mydata['validated_email'] = "email sudah terdaftar"
	else:
		mydata['validated_email'] = "email belum terdaftar"

	# json_now = json.dumps(mydata)
	return JsonResponse(mydata, content_type='application/json', safe=False)

def email_check_empty(request):
	mydata = {'validated_email': ""}
	return JsonResponse(mydata, content_type='application/json', safe=False)

def cekbook_func(request, book):
	URL = 'https://www.googleapis.com/books/v1/volumes?q={}'.format(book)
	REQ = requests.get(url = URL)
	JSON = REQ.json()
	return JsonResponse(JSON)

def sublst_func(request):
	data = serializers.serialize("json", Registration.objects.all())
	json_now = json.dumps(json.loads(data))
	return HttpResponse(data, content_type='application/json')

def sublst_del_func(request):
	email_now = request.POST.get('email_data')
	Registration.objects.all().filter(email=email_now).delete()
	return HttpResponse(email_now)

def login_func(request):
	username = request.POST.get('username')
	password = request.POST.get('password')
	user = authenticate(request, username=username, password=password)
	if user is not None:
		login(request, user)
		res = HttpResponseRedirect(reverse('status_func'))
		res.set_cookie('cookie_enak', 'berhasil!')
		return res

	else:
	# Return an 'invalid login' error message.
	# Sometimes return HTTP 403
		return render(request, "login.html")




