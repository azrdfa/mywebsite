# from django.test import TestCase, Client
# from django.urls import resolve
# from .views import *
# from .models import Status

# # Create your tests here.

# class mybookUnitTest(TestCase):

# 	# url test
# 	def test_story_6_url_is_exist(self):
# 		response = Client().get('/story6/status/')
# 		self.assertEqual(response.status_code,200)

# 	def test_story_6_using_index_func(self):
# 		found = resolve('/story6/status/')
# 		self.assertEqual(found.func, story6_status)

# 	def test_story_6_using_home_template(self):
# 		response = Client().get('/story6/status/')
# 		self.assertTemplateUsed(response, 'status.html')

# 	# model test
# 	def test_model_can_create_new_status(self):
# 		# creating a new status
# 		new_activity = Status.objects.create(Kabar='Aku mau latihan ngoding')

# 		# retrieving all available status
# 		counting_all_available_status = Status.objects.all().count()
# 		self.assertEqual(counting_all_available_status, 1)

# 	def test_can_save_a_POST_request(self):
# 		response = self.client.post('/story6/status/add_activity', data={'Kabar' : 'a'})
# 		counting_all_available_status = Status.objects.all().count()
# 		self.assertEqual(counting_all_available_status, 1)

# 		self.assertEqual(response.status_code, 302)
# 		self.assertEqual(response['location'], '/story6/status/')

# 		new_response = self.client.get('/story6/status/')
# 		html_response = new_response.content.decode('utf8')
# 		self.assertIn('hello', html_response)
