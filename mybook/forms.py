from django import forms
from django.forms import ModelForm
from .models import Registration

class Subscribe_Form(ModelForm):
	class Meta:
		model = Registration
		fields = ['username', 'email', 'password']