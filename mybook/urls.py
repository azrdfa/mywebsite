from django.urls import path
from mybook import views

urlpatterns = [
    path('', views.book_func,  name="book_func"),
    path('json_data/<email>', views.json_data, name="json_data"),
    path('subscriber/', views.subs_func, name="subs_func"),
    path('email_check/', views.email_check_empty, name="email_check_empty"),
    path('email_check/<email>', views.email_check, name="email_check"),
    path('submit_url/', views.submit_func, name="submit_func"),
    path('status/', views.status_func, name="status_func"),
    path('cek_book/<book>', views.cekbook_func, name="cek_book"),
    path('sublst_url/', views.sublst_func, name="sublst_func"),
    path('sublst_del/', views.sublst_del_func, name="sublst_del_func"),
    path('login/', views.login_func, name="login_func")

]
