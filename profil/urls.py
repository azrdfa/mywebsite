from django.urls import path
from profil import views

urlpatterns = [
    path('experience/', views.experience,  name="experience"),
    path('guessbook/', views.guessbook, name="guessbook"),
    path('', views.aboutme, name="aboutme"),
    path('favgames/', views.favgames, name="favgames"),
    path('activity/', views.myschedule, name="activity"),
    path('activity/delete', views.delete_myschedule, name="deleteactivity")
]
