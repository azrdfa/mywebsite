from django import forms
from .models import Jadwal
from django.forms import ModelForm

# textatrbt = {'class':'col-12'}
# dateatrbt = {'class' : 'col-12', 'type' : 'date'}
# timeatrbt = {'class' : 'col-12', 'type' : 'time'}


# class Message_Form(forms.ModelForm):
# 	Kegiatan = forms.CharField(label='Kegiatan',required=True,
# 	 max_length=50, empty_value='nama kegiatan',
# 	 widget=forms.TextInput(attrs=textatrbt))

# 	tempat = forms.CharField(label='tempat',required=True,
# 	 max_length=50, empty_value='nama tempat',
# 	 widget=forms.TextInput(attrs=textatrbt))

# 	kategori = forms.CharField(label='Kegiatan',required=True,
# 	 max_length=50, empty_value='nama kategori',
# 	 widget=forms.TextInput(attrs=textatrbt))

# 	tanggal = forms.DateField(label='tanggal',required=True,
# 	 widget=forms.DateInput(attrs=dateatrbt))

# 	waktu = forms.TimeField(label='waktu', required=True,
# 	widget=forms.TimeInput(attrs=timeatrbt))

class Message_Form(ModelForm):
	class Meta:
		model = Jadwal
		fields = ['Kegiatan','tempat','kategori','tanggal','waktu']