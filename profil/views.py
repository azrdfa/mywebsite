from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Jadwal
from .forms import Message_Form

# Create your views here.
def aboutme (request):
	return render(request, "webazhar.html")
def experience (request):
	return render(request, 'webazharpage2.html')
def guessbook (request):
	return render(request, 'webazharpage3.html')
def favgames (request):
	return render(request, 'webazharpage4.html')

def myschedule(request):
	events = Jadwal.objects.all().values()

	if request.method == "POST":
		form = Message_Form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('activity'))
	else:
		form = Message_Form()

	response = {'form': form, 'events': events}
	html = 'webazharpage5.html'
	return render(request, html, response)

def delete_myschedule(request):
	Jadwal.objects.all().delete()
	return HttpResponseRedirect(reverse('activity'))

